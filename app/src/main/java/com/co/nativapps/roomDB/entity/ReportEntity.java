package com.co.nativapps.roomDB.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "report", indices = {@Index(value = {"picture_name"}, unique = true)})
public class ReportEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "description")
    private String description;

    @NonNull
    @ColumnInfo(name = "picture_name")
    private byte[] pictureName;

    public ReportEntity(@NonNull String description, @NonNull byte[] pictureName) {
        this.description = description;
        this.pictureName = pictureName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public byte[] getPictureName() {
        return pictureName;
    }

    public void setPictureName(@NonNull byte[] pictureName) {
        this.pictureName = pictureName;
    }
}
