package com.co.nativapps.roomDB.dao;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.co.nativapps.reports.Report;
import com.co.nativapps.roomDB.entity.ReportEntity;

import java.util.List;

@Dao
public interface ReportDao {

    @Query("SELECT * FROM 'report' ORDER BY id ASC")
    DataSource.Factory<Integer, ReportEntity>  getReportPagedList();

    @Insert()
    void insertReport(ReportEntity report);

}
