package com.co.nativapps.reports;

public class Report {
    private int id;
    private String description;
    private byte[] pictureName;

    public Report( String description,  byte[] pictureName) {
        this.description = description;
        this.pictureName = pictureName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public  byte[] getPictureName() {
        return pictureName;
    }

    public void setPictureName( byte[] pictureName) {
        this.pictureName = pictureName;
    }
}
