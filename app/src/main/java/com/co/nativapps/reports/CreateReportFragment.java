package com.co.nativapps.reports;

import static android.app.Activity.RESULT_OK;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.co.nativapps.reports.databinding.FragmentCreateReportBinding;
import com.google.android.material.snackbar.Snackbar;

import java.io.ByteArrayOutputStream;

public class CreateReportFragment extends Fragment {

    private FragmentCreateReportBinding mBinding;

    private ReportViewModel mReportViewModel;

    private Handler mHandler;

    private byte[] mImgFormatByte;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mBinding = FragmentCreateReportBinding.inflate(inflater, container, false);

        mReportViewModel = new ViewModelProvider(requireActivity()).get(ReportViewModel.class);

        mHandler = new Handler(Looper.getMainLooper());


        mBinding.ivReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        return mBinding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.btnSaveReport.setOnClickListener(view2 -> {
            if (mImgFormatByte == null)
                Toast.makeText(requireActivity(), R.string.error_complete_take_picture, Toast.LENGTH_SHORT).show();
            else if ( mBinding.tvDescriptionReport.getText().toString().equals(""))
                Toast.makeText(requireActivity(), R.string.error_complete_description_report_field, Toast.LENGTH_SHORT).show();
            else
                callDialog();

        });
    }

    private void callDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.dialog_message).setTitle(R.string.dialog_title);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                try {

                    String reportDescription = mBinding.tvDescriptionReport.getText().toString();

                    mReportViewModel.insertReport(new Report( reportDescription, mImgFormatByte));

                    Snackbar.make(mBinding.getRoot(), R.string.report_registered, Snackbar.LENGTH_SHORT).show();

                    mHandler.postDelayed(runCodeAfterInset, 1500);


                } catch (
                        Exception e) {
                    Toast.makeText(requireActivity(), "Se ha presentado un problema, por favor intente de nuevo", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private Runnable runCodeAfterInset = () -> {
        NavHostFragment.findNavController(this).navigate(R.id.action_CreateReportFragment_to_MainReportFragment);
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            try {

                Bundle extras = data.getExtras();
                Bitmap bitmap = (Bitmap) extras.get("data");

                Glide.with(this).load(bitmap).into(mBinding.ivReport);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    mImgFormatByte = byteArrayOutputStream.toByteArray();
                }


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(requireActivity(), "Se ha presentado un problema, por favor intente de nuevo", Toast.LENGTH_SHORT).show();
            }

        }
    }

}