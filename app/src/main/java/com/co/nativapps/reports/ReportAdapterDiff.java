package com.co.nativapps.reports;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.co.nativapps.roomDB.entity.ReportEntity;

import java.util.Objects;


public class ReportAdapterDiff extends PagedListAdapter<ReportEntity, ReportAdapterDiff.ReportViewHolder> {

    public ReportAdapterDiff() {
        super(DIFF_CALLBACK);

    }

    private static final DiffUtil.ItemCallback<ReportEntity> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<ReportEntity>() {
                // Concert details may have changed if reloaded from the database,// but ID is fixed.
                @Override
                public boolean areItemsTheSame(ReportEntity oldReport, ReportEntity newReport) {
                    return oldReport.getId() == newReport.getId();
                }

                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(@NonNull ReportEntity oldReport, @NonNull ReportEntity newReport) {
                    return oldReport.equals(newReport);
                }
            };

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_list, parent, false);
        return new ReportViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        holder.setReport(Objects.requireNonNull(getItem(position)));
    }

    public static class ReportViewHolder extends RecyclerView.ViewHolder {

        private final View mView;

        public ReportViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView = itemView;
        }

        public void setReport(ReportEntity report) {
            ((TextView) mView.findViewById(R.id.description_text)).setText(report.getDescription());
            ImageView imageView =(ImageView) mView.findViewById(R.id.iv_report);

            ((MainActivity) mView.getContext()).runOnUiThread(() -> {
                byte[] img = report.getPictureName();
                if (img!= null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
                    Glide.with(mView.getContext().getApplicationContext()).asBitmap().load(bitmap).into(imageView);
                }
            });
        }
    }
}