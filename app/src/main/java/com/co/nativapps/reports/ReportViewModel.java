package com.co.nativapps.reports;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedList;

import com.co.nativapps.roomDB.entity.ReportEntity;

public class ReportViewModel extends AndroidViewModel {

    private final LiveData<PagedList<ReportEntity>> mTransactionsDbLivePagedList;

    private ReportRepository mRepository;

    public ReportViewModel(@NonNull Application application) {
        super(application);

        mRepository = new ReportRepository(application);

        this.mTransactionsDbLivePagedList =  mRepository.getReportDbLivePageList();

    }

    public LiveData<PagedList<ReportEntity>> getReportDbLivePagedList() {
        return mTransactionsDbLivePagedList;
    }

    public void insertReport(Report report) {
        mRepository.insertReport(report);
    }

}
