package com.co.nativapps.reports;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.co.nativapps.reports.databinding.FragmentReportMainBinding;


public class ReportMainFragment extends Fragment {

    private FragmentReportMainBinding mBinding;
    private ReportViewModel mReportViewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mBinding = FragmentReportMainBinding.inflate(inflater, container, false);

        mReportViewModel = new ViewModelProvider(requireActivity()).get(ReportViewModel.class);

        setUpRecyclerViewDiff();

        return mBinding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.fab.setOnClickListener(view1 ->
                NavHostFragment.findNavController(ReportMainFragment.this).navigate(R.id.action_ReportMainFragment_to_CreatedReportFragment));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }

    private void setUpRecyclerViewDiff() {

        RecyclerView recyclerView = mBinding.recyclerReport;
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        recyclerView.setHasFixedSize(true);

        ReportAdapterDiff adapter = new ReportAdapterDiff();

        mReportViewModel.getReportDbLivePagedList().observe(requireActivity(), adapter::submitList);

        recyclerView.setAdapter(adapter);
    }


}