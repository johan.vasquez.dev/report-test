package com.co.nativapps.reports;

import android.app.Application;
import android.os.AsyncTask;


import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.co.nativapps.roomDB.AppDatabase;
import com.co.nativapps.roomDB.dao.ReportDao;
import com.co.nativapps.roomDB.entity.ReportEntity;

public class ReportRepository {

    private ReportDao mReportDao;

    public final LiveData mTransactionDbLivePageList;

    public ReportRepository(Application application) {

        AppDatabase db = AppDatabase.getDatabase(application);
        mReportDao = db.reportDao();

        mTransactionDbLivePageList = new LivePagedListBuilder<>(this.mReportDao.getReportPagedList(), 10).build();
    }

    public LiveData<PagedList<ReportEntity>> getReportDbLivePageList() {
        return mTransactionDbLivePageList;
    }

    public void insertReport(Report report) {
        new InsertReportAsyncTask(mReportDao).execute(report);
    }

    public class InsertReportAsyncTask extends AsyncTask<Report ,Void, Void> {

        private ReportDao mAsyncTaskDao;

        public InsertReportAsyncTask(ReportDao dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Report... params) {

            ReportEntity reportEntity = new ReportEntity(params[0].getDescription(), params[0].getPictureName());
            mAsyncTaskDao.insertReport(reportEntity);
            return null;
        }
    }
}
